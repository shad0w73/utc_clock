/* See LICENSE for license details. */
#ifndef SASU_H
#define SASU_H

#include <libnova/ln_types.h>

class sasu
{
public:
	sasu(double lat, double lon);
	void setgeo(double lat, double lon);
	void update();

	double sa() const { return standard.rise; };
	double su() const { return standard.set; };
	double bsa() const { return civil.rise; };
	double bsu() const { return civil.set; };
	double ma() const { return moon.rise; };
	double mu() const { return moon.set; };

private:
	struct ln_lnlat_posn geo;
	struct ln_rst_time civil, standard, moon;

	ln_rst_time normalize_rs_local(struct ln_rst_time rst, double gmtoff);
};

#endif // SASU_H
