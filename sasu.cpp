/* See LICENSE for license details. */
#include <cmath>
#include <ctime>

#include <libnova/solar.h>
#include <libnova/lunar.h>
#include <libnova/julian_day.h>

#include "sasu.h"

sasu::sasu(double lat, double lon)
{
	setgeo(lat, lon);
}

ln_rst_time sasu::normalize_rs_local(struct ln_rst_time rst, double gmtoff)
{
	rst.rise = fmod(rst.rise - 0.5 + gmtoff, 1);
	rst.set  = fmod(rst.set  - 0.5 + gmtoff, 1);

	return rst;
}

void sasu::setgeo(double lat, double lon)
{
	geo.lat = lat;
	geo.lng = lon;

	update();
}

void sasu::update()
{
	time_t now = time(NULL);
	double gmtoff = localtime(&now)->tm_gmtoff / 86400.0;
	double jd = ln_get_julian_from_sys() + gmtoff;

	/* Standart times of sun */
	ln_get_solar_rst_horizon(jd, &geo, LN_SOLAR_STANDART_HORIZON, &standard);
	standard = normalize_rs_local(standard, gmtoff);

	/* Civil times of sun */
	ln_get_solar_rst_horizon(jd, &geo, LN_SOLAR_CIVIL_HORIZON, &civil);
	civil = normalize_rs_local(civil, gmtoff);

	/* Times of moon */
	ln_get_lunar_rst(jd, &geo, &moon);
	moon = normalize_rs_local(moon, gmtoff);
}
