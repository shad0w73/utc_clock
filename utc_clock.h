/* See LICENSE for license details. */
#ifndef UTC_CLOCK_H
#define UTC_CLOCK_H

#include <QWidget>

#include "sasu.h"

class utcClock : public QWidget
{
public:
	utcClock();

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private:
	int b;
	QPoint center;
	QRect clock;
	QRect moon;
	sasu s;

	QPainter *painter;

	template<typename T> T arcLength(T startAngle, T endAngle);
	double f2a(double frac);

	void paintPie(qreal startAngle, qreal endAngle, QColor color);
};

#endif // UTC_CLOCK_H
