/* See LICENSE for license details. */
#include <QApplication>
#include "utc_clock.h"

int main(int argc, char *argv[])
{
	auto *a = new QApplication(argc, argv);
	auto *u = new utcClock();
	int ret = a->exec();
	delete u;
	delete a;
	return ret;
}
