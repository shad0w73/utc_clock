QT += widgets

TARGET = utc_clock

LIBS += -lnova

SOURCES += \
	main.cpp \
	sasu.cpp \
	utc_clock.cpp

HEADERS  += \
	sasu.h \
	utc_clock.h

DISTFILES += \
	LICENSE \
	README.md \
	TODO
