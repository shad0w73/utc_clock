/* See LICENSE for license details. */
#include <cmath>
#include <ctime>

#include <QtWidgets>

#include "utc_clock.h"

static const int diameter = 300;
static const double border_outline_factor = 0.017;
static const double pointer_factor = 0.01;
static const double lat = 49.01953;
static const double lon = 12.09655;

utcClock::utcClock()
	: s(lat, lon)
{
	this->resize(diameter, diameter);
	this->setWindowTitle(QStringLiteral("UTC Clock"));
	this->setPalette(QPalette(QColor(Qt::transparent)));
	this->setWindowFlags(
		Qt::Tool
		| Qt::FramelessWindowHint
		| Qt::NoDropShadowWindowHint
		| Qt::CustomizeWindowHint
	);
	this->show();
}

double utcClock::f2a(double frac)
{
	return fmod((1.25-frac),1)*360;
}

template<typename T>
T utcClock::arcLength(T startAngle, T endAngle)
{
	return endAngle - startAngle + (endAngle < startAngle ? 360 : 0);
}

void utcClock::paintPie(qreal startAngle, qreal endAngle, QColor color)
{
	QPainterPath path(center);
	path.arcTo(clock, startAngle, arcLength(startAngle, endAngle));
	path.lineTo(center);
	painter->fillPath(path, QBrush(color));
}

void utcClock::paintEvent(QPaintEvent *)
{
	QPainterPath path;
	painter = new QPainter(this);
	painter->setRenderHint(QPainter::Antialiasing, true);

	time_t now = time(NULL);
	struct tm *l = localtime(&now);
	now = (l->tm_hour * 60 + l->tm_min) * 60 + l->tm_sec;

	// Background
	paintPie(0, 360, QColor(64, 64, 64));
	for (int i = 0; i < 360; i+=30)
		paintPie(i, i+15, QColor(97, 97, 97));

	// Times
	paintPie(f2a(s.sa()),  f2a(s.bsa()), QColor(255,   0,   0, 127));
	paintPie(f2a(s.su()),  f2a(s.sa()),  QColor(255, 255,   0, 127));
	paintPie(f2a(s.bsu()), f2a(s.su()),  QColor(255,   0,   0, 127));
	paintPie(f2a(s.bsa()), f2a(s.bsu()), QColor(  0,   0, 255, 127));

	// MoonLine
	painter->setPen(QPen(QColor(80, 255, 255), b));
	path = QPainterPath(center);
	path.arcMoveTo(moon, f2a(s.mu()));
	path.arcTo(moon, f2a(s.mu()), arcLength(f2a(s.mu()), f2a(s.ma())));
	painter->drawPath(path);

	// ClockBorder
	painter->setPen(QPen(Qt::black, b));
	path = QPainterPath(center);
	path.arcMoveTo(clock, 0);
	path.arcTo(clock, 0, 360);
	painter->drawPath(path);

	// Pointer
	painter->setPen(QPen(Qt::black, this->width()*pointer_factor));
	path = QPainterPath(center);
	path.arcTo(clock, f2a(now / 86400.0), 0);
	painter->drawPath(path);

	delete painter;
}

void utcClock::resizeEvent(QResizeEvent *)
{
	const auto w = this->width();

	// Base vars
	this->b      = w * border_outline_factor;
	this->center = QPoint(w/2, w/2);
	this->clock  = QRect(b, b, w-2*b, w-2*b);
	this->moon   = QRect(2*b, 2*b, w-4*b, w-4*b);

	// Force fix aspect ratio
	if (w != this->height())
		this->resize(w, w);

	QRegion mask(b/2, b/2, w-b, w-b, QRegion::Ellipse);
	this->setMask(mask);
}

void utcClock::closeEvent(QCloseEvent *)
{
	qApp->quit();
}
